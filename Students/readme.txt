Important!

-> No folders are to be created outside the deliveries folder.

-> Each Student will create his/her own branch (The branch name must be the student first name, lastname, month and year of the module, all separated by underscore 
    ->As example: jesse_silva_11_2017.
    
-> All the deliveries must be sent via Pull request, where the instructor will approve or not the code.
	-> If the code is approved, the student will receive the approval to merge into the master branch