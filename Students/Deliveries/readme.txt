Contains all the work done by the students. Each student must create a folder with his/her first name, lastname, month and year of the module, as example jesse_silva_11_2017. 

The deliveries must be inside this folder and must follow the exact same name of the exercise given by the instructor. 
	* For example, if the instructor gives an exercise called "exercise 1", the name of the file to be delivered would be "Exercise one.java". 
	*If multiple java files are needed, the root folder must carry the name of the exercise.