This repository contains the files for the Java training classes. The folders are as follow:

-> Code Samples
	Contains snippets of code given by the instructor to help on the class
	
-> Exercises
	Contains The exercises given by the instructor for the students to work on
	
-> Materials
	Contains all the handouts, textbooks and extra files to be used by the instructor and students.
	
-> Students
	--> Deliveries
		 Contains all the work done by the students. Each student must create a folder inside this folder with his/her first name, lastname, month and year of the module, 
		 as example jesse_silva_11_2017. The deliveries must be inside this folder and must follow the exact same name as of the exercise given by the instructor.
			* For example, if the instructor gives an exercise called "exercise 1", the name of the file to be delivered would be "Exercise one.java". 
				*If multiple java files are needed, the root folder must carry the name of the exercise.
			
Important!

-> No folders are to be created outside the students/deliveries folder.

-> Each Student will create his/her own branch (The branch name must be the student first name, lastname, month and year of the module, all separated by underscore 
    ->As example: jesse_silva_11_2017.
    
-> All the deliveries must be sent via Pull request, where the instructor will approve or not the code.
	-> If the code is approved, the student will receive the approval to merge into the master branch